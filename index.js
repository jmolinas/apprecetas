const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user');
const recipeSchema = require('./models/recipe');


const app = express();

//Middleware
app.use(express.json());

//MongoDB
// mongoose.connect('mongodb://poli:password@localhost:27017/recipes?authSource=admin')
mongoose.connect('mongodb://poli:password@mongoRecetas:27017/recipes?authSource=admin')
.then(() => console.log('conectado a Mongo'))
.catch((error) => console.error(error))


// Routes
app.post('/new_user', (req,res) => {
    const user = userSchema(req.body);
    user.save()
    .then((data) => {
        console.log('Usuario insertado')
        res.json(data)
    })
    .catch((error) => res.send(error))
    // res.send(req.body)
})

app.post('/new_recipe', (req,res) => {
    const recipe = recipeSchema(req.body);
    recipe.save()
    .then((data) => {
        console.log('Receta insertada')
        res.json(data)
    })
    .catch((error) => res.send(error))
    // res.send(req.body)
})

app.put('/rate', (req,res) => {
    const { recipeId, userId, rating } = req.body
    recipeSchema.updateOne(
        {_id: recipeId},
        [
            { $set: { ratings: {$concatArrays: [{$ifNull: ['$ratings',[]]},[{ userId:userId, rating:rating}]]}}},
            {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0 ]}}}
        ]
    )
    .then((data) => {
        console.log('Rating procesado')
        res.json(data)
    })
    .catch( (error) => {
        console.log(error)
        res.send(error)
    });
    // res.send(req.body)
})

app.get('/recipes', (req,res) => {
    const {userId,recipeId} = req.body
    if (recipeId){
        recipeSchema
            .find({_id : recipeId})
            .then((data) => {
                console.log('Se lista la receta solicitada')
                res.json(data)
            })
            .catch((error)=>res.json({message: error}))
    } else {
        recipeSchema
            .find({userId : userId})
            .then((data) => {
                console.log('Se listan todas las recetas del user')
                res.json(data)
            })
            .catch((error)=>res.json({message: error}))
    }
    // res.send("Enviamos las recetas")
})

app.get('/all_recipes', (req,res) => {
    const {} = req.body
    recipeSchema
        .find({})
        .then((data) => {
            console.log('Se listan todas las recetas creadas')
            res.json(data)
        })
        .catch((error)=>res.json({message: error}))

    // res.send("Enviamos las recetas")
})

app.get('/all_users', (req,res) => {
    const {} = req.body
    userSchema
        .find({})
        .then((data) => {
            console.log('Se listan todos los usuarios creadas')
            res.json(data)
        })
        .catch((error)=>res.json({message: error}))

    // res.send("Enviamos las recetas")
})

app.get('/recipesbyingredient', (req,res) => {
    const {ingredients} = req.body
    const ingredientsArray = ingredients.map(a=>a.name)
    console.log('ingredientsArray')
    console.log(ingredientsArray)
    recipeSchema
        .find({$expr:{$setIsSubset: ["$ingredients.name",ingredientsArray]}})
        .then((data) => {
            console.log('Se listan todas las recetas con los ingredientes solicitado')
            res.json(data)
        })
        .catch((error)=>res.json({message: error}))

    // res.send("Enviamos las recetas")
})

app.listen(3000, () => console.log("Esperando en puerto 3000..."))